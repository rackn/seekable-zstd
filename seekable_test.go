package seekable

import (
	"io"
	"os"
	"testing"

	"github.com/klauspost/compress/zstd"
	"github.com/stretchr/testify/assert"
)

type bytesErr struct {
	tag           uint32
	input         []byte
	expectedBytes []byte
	expectedErr   error
}

func TestIntercompat(t *testing.T) {
	t.Parallel()

	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)

	for _, fn := range []string{
		// t2sz README.md -l 22 -s 1024 -o testdata/intercompat-t2sz.zst
		"testdata/intercompat-t2sz.zst",
		// bazel run //cmd/zstdseek:zstdseek -- \
		//	-f $(realpath README.md) -o $(realpath testdata/intercompat-zstdseek_v0.zst) \
		//	-c 1:1 -t -q 13
		"testdata/intercompat-zstdseek_v0.zst",
	} {
		fn := fn
		t.Run(fn, func(t *testing.T) {
			t.Parallel()

			f, err := os.Open(fn)
			assert.NoError(t, err)
			defer f.Close()
			sz, err := f.Seek(0, io.SeekEnd)
			assert.NoError(t, err)

			rdr, err := NewDecoder(f, sz, dec)
			assert.NoError(t, err)
			defer func() { assert.NoError(t, rdr.Close()) }()
			r := rdr.ReadSeeker()
			buf := make([]byte, 4000)
			n, err := r.Read(buf)
			assert.NoError(t, err)
			assert.Equal(t, 3079, n)
			assert.Equal(t, []byte("  [![License]"), buf[:13])

			i, err := r.Seek(-47, io.SeekEnd)
			assert.NoError(t, err)
			assert.Greater(t, i, int64(1024))

			n, err = rdr.ReadAt(buf, i)
			assert.ErrorIs(t, err, io.EOF)
			assert.Equal(t, 47, n)
			assert.Equal(t, []byte("[license]: https://opensource.org/licenses/MIT\n"), buf[:n])
		})
	}
}
