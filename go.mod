module gitlab.com/rackn/seekable-zstd

go 1.18

require (
	github.com/cespare/xxhash/v2 v2.3.0
	github.com/klauspost/compress v1.16.3
	github.com/stretchr/testify v1.7.2
	gitlab.com/rackn/simplecache v0.0.0-20230324193231-44368de53d93
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
