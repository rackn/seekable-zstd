package seekable

import (
	"encoding/binary"
	"fmt"
	"github.com/cespare/xxhash/v2"
	"io"
	"math"
	"sync"
)

// Encoder implements support for encoding data into a seekable
// zstd stream.  It can be two modes, either as an io.WriteCloser,
// or by using a series of Encode calls followed by an EndStream call.
type Encoder struct {
	enc          ZSTDEncoder
	frameEntries []byte
	frameSize    int
	w            io.Writer

	once *sync.Once
}

func (e *Encoder) frame(idx int) seekTableEntry {
	return e.frameEntries[idx*e.frameSize : (idx+1)*e.frameSize]
}

var (
	_ io.WriteCloser = (*Encoder)(nil)
)

// ZSTDEncoder is the compressor.  Tested with github.com/klauspost/compress/zstd.
type ZSTDEncoder interface {
	EncodeAll(src, dst []byte) []byte
}

// NewWriter wraps the passed io.Writer and Encoder into and indexed ZSTD stream.
// Resulting stream then can be randomly accessed through the ReaderAt and Decoder interfaces.
func NewWriter(w io.Writer, encoder ZSTDEncoder) (*Encoder, error) {
	sw := Encoder{
		once:      &sync.Once{},
		enc:       encoder,
		frameSize: 12,
		w:         w,
	}

	return &sw, nil
}

// Encoder encodes data from source into dest, returning dest with the new conpressed
// data appended to it.  If this would result in violating seekable format constraints,
// an error would be returned instead, and dst will be returned without the additional
// compressed data from src appended to it.
func (s *Encoder) Encode(src, dst []byte) ([]byte, error) {
	if int64(len(src)) > math.MaxUint32 {
		return nil, fmt.Errorf("chunk size too big for seekable format: %d > %d",
			int64(len(src)), int64(math.MaxUint32))
	}

	if len(src) == 0 {
		return nil, nil
	}
	idx := int64(len(dst))
	dst = s.enc.EncodeAll(src, dst)
	idx = int64(len(dst)) - idx
	if idx > math.MaxUint32 {
		return dst[:idx], fmt.Errorf("result size too big for seekable format: %d > %d",
			idx, int64(math.MaxUint32))
	}

	entry := makeSeekTableEntry(uint32(idx), uint32(len(src)), uint32((xxhash.Sum64(src)<<32)>>32))
	s.frameEntries = append(s.frameEntries, entry...)

	return dst, nil
}

// EndStream appends the seek table to dest, returning dst.
// If there are too many seek frames, an error will be returned.
func (s *Encoder) EndStream(dst []byte) ([]byte, error) {
	finalSize := int64(8 + len(s.frameEntries) + 9)
	if finalSize > math.MaxUint32 {
		return dst, fmt.Errorf("requested skippable frame size (%d) > max uint32 ", finalSize)
	}
	var buf [12]byte
	binary.LittleEndian.PutUint32(buf[:4], skippableFrameMagic+seekableTag)
	binary.LittleEndian.PutUint32(buf[4:8], uint32(finalSize-8))
	dst = append(dst, buf[:8]...)
	dst = append(dst, s.frameEntries...)
	makeSeekTableFooter(buf[:], uint32(len(s.frameEntries)/s.frameSize), 0x80)
	dst = append(dst, buf[:9]...)
	return dst, nil
}

// Write implements io.Writer for Encoder.  Each call to Write will encode
// a single seekable segment.
func (s *Encoder) Write(src []byte) (int, error) {
	dst, err := s.Encode(src, nil)
	if err != nil {
		return 0, err
	}

	n, err := s.w.Write(dst)
	if err != nil {
		return 0, err
	}
	if n != len(dst) {
		return 0, fmt.Errorf("partial write: %d out of %d", n, len(dst))
	}

	return len(src), nil
}

func (s *Encoder) writeSeekTable() error {
	seekTableBytes, err := s.EndStream(nil)
	if err != nil {
		return err
	}
	_, err = s.w.Write(seekTableBytes)
	return err
}

// Close closes the stream, writing out the seek table.  The underlying
// io.Writer must be Closed separately.
func (s *Encoder) Close() (err error) {
	s.once.Do(func() {
		err = s.writeSeekTable()
	})
	return
}
