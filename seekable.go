package seekable

import (
	"encoding/binary"
	"fmt"
)

const (
	/*
		The format consists of a number of frames (Zstandard compressed frames and skippable frames), followed by a final skippable frame at the end containing the seek table.

		Seek Table Format

		The structure of the seek table frame is as follows:

			|`Skippable_Magic_Number`|`Frame_Size`|`[Seek_Table_Entries]`|`Seek_Table_Footer`|
			|------------------------|------------|----------------------|-------------------|
			| 4 bytes                | 4 bytes    | 8-12 bytes each      | 9 bytes           |

		Skippable_Magic_Number

		Value: 0x184D2A5E.
		This is for compatibility with Zstandard skippable frames: https://github.com/facebook/zstd/blob/release/doc/zstd_compression_format.md#skippable-frames.

		Since it is legal for other Zstandard skippable frames to use the same
		magic number, it is not recommended for a decoder to recognize frames
		solely on this.

		Frame_Size

		The total size of the skippable frame, not including the `Skippable_Magic_Number` or `Frame_Size`.
		This is for compatibility with Zstandard skippable frames: https://github.com/facebook/zstd/blob/release/doc/zstd_compression_format.md#skippable-frames.

		https://github.com/facebook/zstd/blob/dev/contrib/seekable_format/zstd_seekable_compression_format.md
	*/
	skippableFrameMagic = 0x184D2A50

	seekableMagicNumber = 0x8F92EAB1

	seekTableFooterOffset = 9

	frameSizeFieldSize            = 4
	skippableMagicNumberFieldSize = 4

	// maxFrameSize is the maximum framesize supported by decoder.  This is to prevent OOMs due to untrusted input.
	maxDecoderFrameSize = 128 << 20

	seekableTag = 0xE
)

/*
seekTableFlags describes the format of the seek table.

	| Bit number | Field name                |
	| ---------- | ----------                |
	| 7          | `Checksum_Flag`           |
	| 6-2        | `Reserved_Bits`           |
	| 1-0        | `Unused_Bits`             |

While only `Checksum_Flag` currently exists, there are 7 other bits in this field that can be used for future changes to the format,
for example the addition of inline dictionaries.

`Reserved_Bits` are not currently used but may be used in the future for breaking changes,
so a compliant decoder should ensure they are set to 0.

`Unused_Bits` may be used in the future for non-breaking changes,
so a compliant decoder should not interpret these bits.
*/
type seekTableFlags byte

func (s seekTableFlags) checksum() bool {
	return s&0x80 > 0
}

/*
seekTableFooter is the footer of a seekable ZSTD stream.

The seek table footer format is as follows:

	|`Number_Of_Frames`|`Seek_Table_Descriptor`|`Seekable_Magic_Number`|
	|------------------|-----------------------|-----------------------|
	| 4 bytes          | 1 byte                | 4 bytes               |

https://github.com/facebook/zstd/blob/dev/contrib/seekable_format/zstd_seekable_compression_format.md#seek_table_footer
*/
type seekTableFooter []byte

// frameCount is the total number of data frames in the zstd archive.
func (s seekTableFooter) frameCount() uint32 {
	return binary.LittleEndian.Uint32(s[:4])
}

// flags is the descriptor for the seek table.
func (s seekTableFooter) flags() seekTableFlags {
	return seekTableFlags(s[4])
}

// magic is the magic number that indicates that archive is in fact seekable.
func (s seekTableFooter) magic() uint32 {
	return binary.LittleEndian.Uint32(s[5:])
}

func (s seekTableFooter) Valid() error {
	if len(s) != seekTableFooterOffset {
		return fmt.Errorf("footer length mismatch: want %d, have %d", seekTableFooterOffset, len(s))
	}
	m := s.magic()
	if m != uint32(seekableMagicNumber) {
		return fmt.Errorf("footer magic mismatch: want %08x, havr %08x", uint32(seekableMagicNumber), m)
	}
	if s.flags()&0b01111100 > 0 {
		return fmt.Errorf("footer reserved bits set")
	}
	return nil
}

func makeSeekTableFooter(buf []byte, frameCount uint32, flags seekTableFlags) {
	buf[4] = byte(flags)
	binary.LittleEndian.PutUint32(buf[:4], frameCount)
	binary.LittleEndian.PutUint32(buf[5:], uint32(seekableMagicNumber))
}

/*
seekTableEntry is an element of the Seek Table describing each of the ZSTD-compressed frames in the stream.

`Seek_Table_Entries` consists of `Number_Of_Frames` (one for each frame in the data, not including the seek table frame) entries of the following form, in sequence:

	|`Compressed_Size`|`Decompressed_Size`|`[checksum]`|
	|-----------------|-------------------|------------|
	| 4 bytes         | 4 bytes           | 4 bytes    |

https://github.com/facebook/zstd/blob/dev/contrib/seekable_format/zstd_seekable_compression_format.md#seek_table_entries
*/
type seekTableEntry []byte

func makeSeekTableEntry(csize, decsize, cksum uint32) (res seekTableEntry) {
	res = make(seekTableEntry, 12)
	binary.LittleEndian.PutUint32(res[:4], csize)
	binary.LittleEndian.PutUint32(res[4:8], decsize)
	binary.LittleEndian.PutUint32(res[8:12], cksum)
	return res
}

// cSize is the compressed size of the frame.
// The cumulative sum of the `Compressed_Size` fields of frames `0` to `i` gives the offset in the compressed file of frame `i+1`.
func (e seekTableEntry) cSize() uint32 {
	return binary.LittleEndian.Uint32(e[:4])
}

// dSize is the size of the decompressed data contained in the frame.
// For skippable or otherwise empty frames, this value is 0.
func (e seekTableEntry) dSize() uint32 {
	return binary.LittleEndian.Uint32(e[4:8])
}

// checksum is the checksum of the uncompresed data in the frame.  It is only meaningful if
// checksum is set in the seek table descriptor.
func (e seekTableEntry) checksum() uint32 {
	if len(e) == 12 {
		return binary.LittleEndian.Uint32(e[8:12])
	}
	return 0
}
