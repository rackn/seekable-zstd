package seekable

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math/bits"
	"sort"
	"sync"
	"sync/atomic"

	"github.com/cespare/xxhash/v2"
	c "gitlab.com/rackn/simplecache"
)

var (
	_ io.ReaderAt = &Decoder{}
	_ io.Closer   = &Decoder{}
)

// Decoder implements an io.ReaderAt for a seekable zstd stream.
type Decoder struct {
	dec   ZSTDDecoder
	index []frame

	checksums bool

	numFrames int64
	endOffset int64
	rs        io.ReaderAt
	sz        int64

	closed uint32
	bufs   *sync.Pool
	cache  *c.Cache[uint64, []byte]
}

func (d *Decoder) readFooter() ([]byte, error) {
	buf := make([]byte, seekTableFooterOffset)
	_, err := d.rs.ReadAt(buf, d.sz-seekTableFooterOffset)
	if err != nil {
		return nil, fmt.Errorf("failed to read footer at: %d: %w", d.sz-seekTableFooterOffset, err)
	}

	return buf, nil
}

func (d *Decoder) readSkipFrame(skippableFrameOffset int64) ([]byte, error) {
	buf := make([]byte, skippableFrameOffset)
	_, err := d.rs.ReadAt(buf, d.sz-skippableFrameOffset)
	if err != nil {
		return nil, fmt.Errorf("failed to read skippable frame header at: %d: %w", d.sz-skippableFrameOffset, err)
	}
	return buf, nil
}

// Size returns the total uncompressed size of the encoded data.
func (r *Decoder) Size() int64 {
	return r.endOffset
}

// NumFrames returns the total number of encoded data frames making up the encoded data.
func (r *Decoder) NumFrames() int64 {
	return r.numFrames
}

func (r *Decoder) getFrameForOffset(off uint64) *frame {
	idx, found := sort.Find(len(r.index), func(i int) int {
		if off >= r.index[i].dOffset+uint64(r.index[i].dSize) {
			return 1
		} else if off < r.index[i].dOffset {
			return -1
		} else {
			return 0
		}
	})
	if !found {
		return nil
	}
	return &r.index[idx]
}

func (r *Decoder) getFrame(id int64) (found *frame) {
	if id < 0 || id >= int64(len(r.index)) {
		return nil
	}
	return &r.index[id]
}

func (r *Decoder) getBuf() []byte {
	return r.bufs.Get().([]byte)[:0]
}

func (r *Decoder) putBuf(b []byte) {
	r.bufs.Put(b[:cap(b)])
}

// ZSTDDecoder is the decompressor.  Tested with github.com/klauspost/compress/zstd.
type ZSTDDecoder interface {
	DecodeAll(input, dst []byte) ([]byte, error)
}

// NewDecoder returns ZSTD stream reader that can be randomly accessed using uncompressed data offset.
func NewDecoder(rs io.ReaderAt, sz int64, decoder ZSTDDecoder) (*Decoder, error) {
	bufs := &sync.Pool{New: func() any { return make([]byte, 0, 512) }}
	sr := Decoder{
		dec:  decoder,
		rs:   rs,
		sz:   sz,
		bufs: bufs,
	}

	idx, err := sr.indexFooter()
	if err != nil {
		return nil, err
	}

	sr.index = idx
	if len(idx) > 0 {
		sr.numFrames = int64(len(idx))
		lastFrame := len(idx) - 1
		sr.endOffset = int64(idx[lastFrame].dOffset) + int64(idx[lastFrame].dSize)
	} else {
		sr.endOffset = 0
		sr.numFrames = 0
	}
	sr.cache = c.New[uint64, []byte](64-bits.LeadingZeros64(uint64(sr.numFrames)), func(b []byte) { bufs.Put(b[:0]) })

	return &sr, nil
}

func (r *Decoder) ReadSeeker() io.ReadSeeker {
	return io.NewSectionReader(r, 0, r.Size())
}

func (r *Decoder) ReadAt(p []byte, off int64) (n int, err error) {
	for m := 0; n < len(p) && err == nil; n += m {
		m, err = r.read(p[n:], off+int64(n))
	}
	return
}

func (r *Decoder) Close() error {
	if atomic.CompareAndSwapUint32(&r.closed, 0, 1) {
		r.cache.Clear()
		r.index = nil
	}
	return nil
}

func (r *Decoder) read(dst []byte, off int64) (readSize int, err error) {
	if atomic.LoadUint32(&r.closed) > 0 {
		err = fmt.Errorf("reader is closed")
		return
	}

	if off >= r.endOffset {
		err = io.EOF
		return
	}
	if off < 0 {
		err = fmt.Errorf("offset before the start of the file: %d", off)
		return
	}

	index := r.getFrameForOffset(uint64(off))
	if index == nil {
		err = fmt.Errorf("failed to get index by offset: %d", off)
		return
	}
	if off < int64(index.dOffset) || off > int64(index.dOffset)+int64(index.dSize) {
		err = fmt.Errorf("offset outside of index bounds: %d: min: %d, max: %d",
			off, int64(index.dOffset), int64(index.dOffset)+int64(index.dSize))
		return
	}
	var decompressed []byte
	decompressed, err = r.cache.Acquire(index.dOffset, func(offset uint64) ([]byte, error) {
		// slowpath
		if index.cSize > maxDecoderFrameSize {
			return nil, fmt.Errorf("index.cSize is too big: %d > %d",
				index.cSize, maxDecoderFrameSize)
		}
		p := r.getBuf()
		if cap(p) < int(index.dSize) {
			r.putBuf(p)
			p = make([]byte, index.cSize)
		} else {
			p = p[:index.cSize]
		}
		defer r.putBuf(p)
		_, pErr := r.rs.ReadAt(p, int64(index.cOffset))
		if errors.Is(pErr, io.EOF) {
			pErr = nil
		}
		if pErr != nil {
			return nil, fmt.Errorf("failed to read compressed data at: %d, %w", index.cOffset, pErr)
		}

		if len(p) != int(index.cSize) {
			return nil, fmt.Errorf("compressed size does not match index at: %d: expected: %d, index: %+v",
				off, len(p), index)
		}
		var dec []byte
		dec, pErr = r.dec.DecodeAll(p, r.getBuf())
		if pErr != nil {
			return nil, pErr
		}
		if r.checksums {
			checksum := uint32((xxhash.Sum64(dec) << 32) >> 32)
			if index.checksum != checksum {
				pErr = fmt.Errorf("checksum verification failed at: %d: expected: %d, actual: %d",
					index.cOffset, index.checksum, checksum)
				return nil, pErr
			}
		}

		if len(dec) != int(index.dSize) {
			pErr = fmt.Errorf("index corruption: len: %d, expected: %d", len(dec), int(index.dSize))
			return nil, pErr
		}
		return dec, nil
	})
	if err != nil {
		return
	}
	defer r.cache.Release(index.dOffset)
	offsetWithinFrame := uint64(off) - index.dOffset

	size := uint64(len(decompressed)) - offsetWithinFrame
	if size > uint64(len(dst)) {
		size = uint64(len(dst))
	}

	return copy(dst, decompressed[offsetWithinFrame:offsetWithinFrame+size]), nil
}

func (r *Decoder) indexFooter() ([]frame, error) {
	// read seekTableFooter
	buf, err := r.readFooter()
	if err != nil {
		return nil, fmt.Errorf("failed to read footer: %w", err)
	}
	if len(buf) < seekTableFooterOffset {
		return nil, fmt.Errorf("footer is too small: %d", len(buf))
	}

	// parse seekTableFooter
	footer := seekTableFooter(buf[len(buf)-seekTableFooterOffset:])
	if err = footer.Valid(); err != nil {
		return nil, err
	}

	r.checksums = footer.flags().checksum()

	// read SeekTableEntries
	seekTableEntrySize := int64(8)
	if footer.flags().checksum() {
		seekTableEntrySize += 4
	}

	skippableFrameOffset := seekTableFooterOffset + seekTableEntrySize*int64(footer.frameCount())
	skippableFrameOffset += frameSizeFieldSize
	skippableFrameOffset += skippableMagicNumberFieldSize

	if skippableFrameOffset > maxDecoderFrameSize {
		return nil, fmt.Errorf("frame offset is too big: %d > %d",
			skippableFrameOffset, maxDecoderFrameSize)
	}

	buf, err = r.readSkipFrame(skippableFrameOffset)
	if err != nil {
		return nil, fmt.Errorf("failed to read footer: %w", err)
	}

	if len(buf) < frameSizeFieldSize+skippableMagicNumberFieldSize+seekTableFooterOffset {
		return nil, fmt.Errorf("skip frame is too small: %d", len(buf))
	}

	// parse SeekTableEntries
	magic := binary.LittleEndian.Uint32(buf[0:4])
	if magic != skippableFrameMagic+seekableTag {
		return nil, fmt.Errorf("skippable frame magic mismatch %d vs %d",
			magic, skippableFrameMagic+seekableTag)
	}

	expectedFrameSize := int64(len(buf)) - frameSizeFieldSize - skippableMagicNumberFieldSize
	frameSize := int64(binary.LittleEndian.Uint32(buf[4:8]))
	if frameSize != expectedFrameSize {
		return nil, fmt.Errorf("skippable frame size mismatch: expected: %d, actual: %d",
			expectedFrameSize, frameSize)
	}

	if frameSize > maxDecoderFrameSize {
		return nil, fmt.Errorf("frame is too big: %d > %d", frameSize, maxDecoderFrameSize)
	}

	return r.frames(buf[8:len(buf)-seekTableFooterOffset], uint64(seekTableEntrySize))
}

func (r *Decoder) frames(p []byte, entrySize uint64) ([]frame, error) {
	if uint64(len(p))%entrySize != 0 {
		return nil, fmt.Errorf("seek table size is not multiple of %d", entrySize)
	}

	res := make([]frame, int(uint64(len(p))/entrySize))
	var entry seekTableEntry
	var compOffset, decompOffset uint64

	var i int64
	for indexOffset := uint64(0); indexOffset < uint64(len(p)); indexOffset += entrySize {
		entry = p[indexOffset : indexOffset+entrySize]
		res[i].cOffset = compOffset
		res[i].dOffset = decompOffset
		res[i].cSize = entry.cSize()
		res[i].dSize = entry.dSize()
		res[i].checksum = entry.checksum()
		compOffset += uint64(entry.cSize())
		decompOffset += uint64(entry.dSize())
		i++
	}

	return res, nil
}

// frame is the post-proccessed view of the Seek_Table_Entries suitable for indexing.
type frame struct {
	// cOffset is the offset within compressed stream.
	cOffset uint64
	// dOffset is the offset within decompressed stream.
	dOffset uint64
	// cSize is the size of the compressed frame.
	cSize uint32
	// dSize is the size of the original data.
	dSize uint32
	// checksum is the lower 32 bits of the XXH64 hash of the uncompressed data.
	checksum uint32
}
