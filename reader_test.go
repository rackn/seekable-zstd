package seekable

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
	"testing"

	"github.com/klauspost/compress/zstd"
	"github.com/stretchr/testify/assert"
)

const sourceString = "testtest2"

var checksum = []byte{
	// frame 1
	0x28, 0xb5, 0x2f, 0xfd, 0x04, 0x00, 0x21, 0x00, 0x00,
	// "test"
	0x74, 0x65, 0x73, 0x74,
	0x39, 0x81, 0x67, 0xdb,
	// frame 2
	0x28, 0xb5, 0x2f, 0xfd, 0x04, 0x00, 0x29, 0x00, 0x00,
	// "test2"
	0x74, 0x65, 0x73, 0x74, 0x32,
	0x87, 0xeb, 0x11, 0x71,
	// skippable frame
	0x5e, 0x2a, 0x4d, 0x18,
	0x21, 0x00, 0x00, 0x00,
	// index
	0x11, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x39, 0x81, 0x67, 0xdb,
	0x12, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x87, 0xeb, 0x11, 0x71,
	// footer
	0x02, 0x00, 0x00, 0x00,
	0x80,
	0xb1, 0xea, 0x92, 0x8f,
}

var noChecksum = []byte{
	// frame 1
	0x28, 0xb5, 0x2f, 0xfd, 0x04, 0x00, 0x21, 0x00, 0x00,
	// "test"
	0x74, 0x65, 0x73, 0x74,
	0x39, 0x81, 0x67, 0xdb,
	// frame 2
	0x28, 0xb5, 0x2f, 0xfd, 0x04, 0x00, 0x29, 0x00, 0x00,
	// "test2"
	0x74, 0x65, 0x73, 0x74, 0x32,
	0x87, 0xeb, 0x11, 0x71,
	// skippable frame
	0x5e, 0x2a, 0x4d, 0x18,
	0x19, 0x00, 0x00, 0x00,
	// index
	0x11, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
	0x12, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00,
	// footer
	0x02, 0x00, 0x00, 0x00,
	0x00,
	0xb1, 0xea, 0x92, 0x8f,
}

func TestReader(t *testing.T) {
	t.Parallel()

	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)
	defer dec.Close()

	for _, b := range [][]byte{checksum, noChecksum} {
		br := bytes.NewReader(b)
		rdr, err := NewDecoder(br, br.Size(), dec)
		assert.NoError(t, err)
		r := rdr.ReadSeeker()
		assert.Equal(t, int64(9), rdr.endOffset)
		assert.Equal(t, 2, len(rdr.index))

		bytes1 := []byte("test")
		bytes2 := []byte("test2")

		tmp := make([]byte, 4096)
		n, err := r.Read(tmp)
		assert.NoError(t, err)
		assert.Equal(t, len(bytes1)+len(bytes2), n)
		assert.Equal(t, bytes1, tmp[:4])
		assert.Equal(t, bytes2, tmp[4:n])

		_, err = r.Read(tmp)
		assert.Equal(t, err, io.EOF)

		err = rdr.Close()
		assert.NoError(t, err)

		// read after close
		_, err = rdr.ReadAt(tmp, 0)
		assert.ErrorContains(t, err, "reader is closed")

		// double close
		err = rdr.Close()
		assert.NoError(t, err)
	}
}

func TestReaderEdges(t *testing.T) {
	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)

	source := []byte(sourceString)
	for i, b := range [][]byte{checksum, noChecksum} {
		b := b
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()

			sr := bytes.NewReader(b)
			rdr, err := NewDecoder(sr, sr.Size(), dec)
			assert.NoError(t, err)
			defer func() { assert.NoError(t, rdr.Close()) }()
			r := rdr.ReadSeeker()

			for _, whence := range []int{io.SeekStart, io.SeekEnd} {
				for n := int64(-1); n <= int64(len(source))+1; n++ {
					for m := int64(0); m <= int64(len(source))+1; m++ {
						var j int64
						switch whence {
						case io.SeekStart:
							j, err = r.Seek(n, whence)
						case io.SeekEnd:
							j, err = r.Seek(int64(-len(source))+n, whence)
						}
						if n < 0 {
							assert.Error(t, err)
							continue
						}
						assert.NoError(t, err)
						assert.Equal(t, n, j)

						tmp := make([]byte, m)
						k, err := r.Read(tmp)
						if n >= int64(len(source)) {
							assert.Equal(t, err, io.EOF,
								"%d: should return EOF at %d, len(source): %d, len(tmp): %d, k: %d, whence: %d",
								i, n, len(source), m, k, whence)
							continue
						} else {
							assert.NoError(t, err,
								"%d: should NOT return EOF at %d, len(source): %d, len(tmp): %d, k: %d, whence: %d",
								i, n, len(source), m, k, whence)
						}

						assert.Equal(t, source[n:n+int64(k)], tmp[:k])
					}
				}
			}
		})
	}
}

// TestReaderAt verified the following ReaderAt asssumption:
//
//	When ReadAt returns n < len(p), it returns a non-nil error explaining why more bytes were not returned.
//	In this respect, ReadAt is stricter than Read.
func TestReaderAt(t *testing.T) {
	t.Parallel()

	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)
	defer dec.Close()

	for _, sr := range []*bytes.Reader{
		bytes.NewReader(noChecksum),
		bytes.NewReader(noChecksum),
	} {
		sr := sr
		t.Run(fmt.Sprintf("%T", sr), func(t *testing.T) {
			rdr, err := NewDecoder(sr, sr.Size(), dec)
			assert.NoError(t, err)
			defer func() { assert.NoError(t, rdr.Close()) }()
			r := rdr.ReadSeeker()

			oldOffset, err := r.Seek(0, io.SeekCurrent)
			assert.NoError(t, err)
			assert.Equal(t, int64(0), oldOffset)

			tmp1 := make([]byte, 3)
			k1, err := rdr.ReadAt(tmp1, 3)
			assert.NoError(t, err)
			assert.Equal(t, 3, k1)
			assert.Equal(t, []byte("tte"), tmp1)

			// If ReadAt is reading from an input source with a seek offset,
			// ReadAt should not affect nor be affected by the underlying seek offset.
			newOffset, err := r.Seek(0, io.SeekCurrent)
			assert.NoError(t, err)
			assert.Equal(t, newOffset, oldOffset)

			tmp2 := make([]byte, 100)
			k2, err := rdr.ReadAt(tmp2, 3)
			assert.Error(t, err, io.EOF)

			tmp_last := make([]byte, 1)
			k_last, err := rdr.ReadAt(tmp_last, 8)
			assert.Equal(t, 1, k_last)
			assert.Equal(t, []byte("2"), tmp_last)
			assert.NoError(t, err)

			tmp_oob := make([]byte, 1)
			_, err = rdr.ReadAt(tmp_oob, 9)
			assert.Error(t, err, io.EOF)

			assert.Equal(t, 6, k2)
			assert.Equal(t, []byte("ttest2"), tmp2[:k2])

			sectionReader := io.NewSectionReader(rdr, 3, 4)
			tmp3, err := io.ReadAll(sectionReader)
			assert.NoError(t, err)
			assert.Equal(t, 4, len(tmp3))
			assert.Equal(t, []byte("ttes"), tmp3)
		})
	}
}

func TestReaderEdgesParallel(t *testing.T) {
	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)

	source := []byte(sourceString)
	for i, b := range [][]byte{checksum, noChecksum} {
		b := b

		sr := bytes.NewReader(b)
		r, err := NewDecoder(sr, sr.Size(), dec)
		assert.NoError(t, err)

		for n := int64(-1); n <= int64(len(source)); n++ {
			for m := int64(0); m <= int64(len(source)); m++ {
				t.Run(fmt.Sprintf("%d/len:%d/buf:%d", i, n, m), func(t *testing.T) {
					t.Parallel()

					tmp := make([]byte, m)
					k, err := r.ReadAt(tmp, n)
					if n < 0 && m != 0 {
						assert.Error(t, err,
							"%d: should return Error at %d: ret: %d, bytes: %+v",
							i, n, k, tmp)
						return
					}

					if m == 0 {
						assert.NoError(t, err)
						assert.Equal(t, 0, k)
						assert.Equal(t, make([]byte, m), tmp)
						return
					}

					if n >= int64(len(source)) {
						assert.Equal(t, err, io.EOF,
							"%d: should return EOF at %d, len(source): %d, len(tmp): %d, k: %d",
							i, n, len(source), m, k)
						assert.Equal(t, 0, k, "should not read anything at the end")
						return
					}
					if n+m <= int64(len(source)) {
						assert.NoError(t, err,
							"%d: should NOT return Err at %d, len(source): %d, len(tmp): %d, k: %d",
							i, n, len(source), m, k)
					} else {
						assert.Equal(t, err, io.EOF,
							"%d: should return EOF at %d, len(source): %d, len(tmp): %d, k: %d",
							i, n, len(source), m, k)
					}

					assert.Equal(t, source[n:n+int64(k)], tmp[:k])
				})
			}
		}
	}
}

func TestNoReaderAt(t *testing.T) {
	t.Parallel()

	dec, err := zstd.NewReader(nil)
	assert.NoError(t, err)
	defer dec.Close()

	for _, sr := range []*bytes.Reader{
		bytes.NewReader(checksum),
		bytes.NewReader(noChecksum),
	} {
		sr := sr
		t.Run(fmt.Sprintf("%T", sr), func(t *testing.T) {
			rdr, err := NewDecoder(sr, sr.Size(), dec)
			assert.NoError(t, err)
			defer func() { assert.NoError(t, rdr.Close()) }()
			r := rdr.ReadSeeker()

			tmp := make([]byte, 3)
			n, err := rdr.ReadAt(tmp, 5)
			assert.NoError(t, err)
			assert.Equal(t, 3, n)
			assert.Equal(t, tmp[:n], []byte("est"))

			// If ReadAt is reading from an input source with a seek offset,
			// ReadAt should not affect nor be affected by the underlying seek offset.
			m, err := r.Seek(0, io.SeekCurrent)
			assert.NoError(t, err)
			assert.Equal(t, int64(0), m)

			tmp = make([]byte, 4096)
			n, err = r.Read(tmp)
			assert.NoError(t, err)
			assert.Equal(t, 9, n)
			assert.Equal(t, tmp[:4], []byte("test"))
			assert.Equal(t, tmp[4:n], []byte("test2"))

			m, err = r.Seek(-4, io.SeekCurrent)
			assert.NoError(t, err)
			assert.Equal(t, int64(5), m)

			n, err = r.Read(tmp)
			assert.NoError(t, err)
			assert.Equal(t, 4, n)
			assert.Equal(t, tmp[:n], []byte("est2"))

			_, err = r.Seek(-1, io.SeekStart)
			assert.ErrorContains(t, err, "invalid offset")

			_, err = r.Seek(0, 9999)
			assert.Errorf(t, err, "unknown whence: %d", 9999)

			_, err = r.Seek(999, io.SeekStart)
			assert.NoError(t, err)

			_, err = r.Read(tmp)
			assert.ErrorIs(t, err, io.EOF)
		})
	}
}

func TestEmptyWriteRead(t *testing.T) {
	t.Parallel()

	enc, err := zstd.NewWriter(nil, zstd.WithEncoderLevel(zstd.SpeedFastest))
	assert.NoError(t, err)

	var b bytes.Buffer
	bw := io.Writer(&b)
	w, err := NewWriter(bw, enc)
	assert.NoError(t, err)

	bytes1 := []byte("")
	bytesWritten1, err := w.Write(bytes1)
	assert.NoError(t, err)
	assert.Equal(t, 0, bytesWritten1)

	err = w.Close()
	assert.NoError(t, err)

	dec1, err := zstd.NewReader(nil)
	assert.NoError(t, err)

	// test seekable decompression
	compressed := b.Bytes()

	sr := bytes.NewReader(compressed)
	rdr, err := NewDecoder(sr, sr.Size(), dec1)
	assert.NoError(t, err)
	defer func() { assert.NoError(t, rdr.Close()) }()
	r := rdr.ReadSeeker()
	tmp1 := make([]byte, 1)
	n, err := r.Read(tmp1)
	assert.Equal(t, err, io.EOF)
	assert.Equal(t, 0, n)

	// test native decompression
	dec2, err := zstd.NewReader(bytes.NewReader(compressed))
	assert.NoError(t, err)
	defer dec2.Close()

	tmp2 := make([]byte, 1)
	n, err = dec2.Read(tmp2)
	assert.Equal(t, err, io.EOF)
	assert.Equal(t, 0, n)
}

func TestSeekTableParsing(t *testing.T) {
	var err error

	t.Parallel()

	// checksum.
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		1 << 7,
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.NoError(t, err)

	// No checksum.
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		0x00,
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.NoError(t, err)

	// Unused bits.
	assert.NoError(t, err)
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		(1 << 7) + 0x01 + 0x2,
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.NoError(t, err)

	// Reserved bits.
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		0x84,
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.ErrorContains(t, err, "footer reserved bits")
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		0x80 + 0x40,
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.ErrorContains(t, err, "footer reserved bits")

	// Size.
	err = seekTableFooter{
		0xb1, 0xea, 0x92, 0x8f,
	}.Valid()
	assert.ErrorContains(t, err, "footer length mismatch")

	// Magic.
	err = seekTableFooter{
		0x00, 0x00, 0x00, 0x00,
		0x80,
		0xea, 0x92, 0x8f, 0xb1,
	}.Valid()
	assert.ErrorContains(t, err, "footer magic mismatch")
}
